package app.example.org.e5app.ui.Editar_Perfil;

//Autor: Manuel Mouzo Marsella

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class EditarPerfilViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public EditarPerfilViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is editar perfil fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
