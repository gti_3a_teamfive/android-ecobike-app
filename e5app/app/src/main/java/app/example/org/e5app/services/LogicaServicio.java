/***************************************************************************************************
 *      LOGICA DEL SERVICIO MEASURESUPDATE
 *
 *      Recibe las medidas por bluetooth, recibe la localización GPS
 *      y manda las medidas a la API REST del servidor
 *
 *      autor: Joan Lluís Fuster
***************************************************************************************************/

package app.example.org.e5app.services;

import android.content.Context;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import app.example.org.e5app.models.Measure;

import java.util.ArrayList;

import org.json.*;

/**************************************************************************************************/

/**************************************************************************************************/
public class LogicaServicio implements ReceptorBLE.RecibirResultadoBLE {

    private static final String TAG = "LogicaServicio";

    /**
     * The context is required by ReceptorBLE and LocalizadorGPS
     */
    private Context mContext;

    /**
     * The ReceptorBLE scans for measures from sensor
     */
    private ReceptorBLE miReceptorBLE;

    /**
     * LocalizadorGPS para obtener la posición de la medida
     */
    private LocalizadorGPS localizadorGPS;

    /**
     * The current measure.
     */
    private Measure measure;

    /**
     * The current location.
     */
    private Location location;

    /**
     * Intervalo entre comprobación de movimiento
     * Cada 10 intervalos pide si no hay movimiento
     */
    private final int interval = 10000; // 10 Seconds

    static final String SENSOR_SERIAL_NUMBER = "sensor_serial_number";

    /**
     * Variables usadas para la calibración
     */

     double factorDeCalibracion=1;
     ArrayList<Integer> medidasDeHoy;
     int medidasContador=0;

    /**********************************************************************************************/
    /**
     * Interfaz para recibir las medidas
     */
    /**********************************************************************************************/
    ReceptorBLE.RecibirResultadoBLE recibirResultadoBLE = new ReceptorBLE.RecibirResultadoBLE() {
        @Override
        public void onRecibirResultadoBLE(ResultadoBLE resultadoBLE) {
            Log.d(TAG, "onRecibirResultadoBLE: RECIBIDO: " + resultadoBLE.toString());

            location = localizadorGPS.obtenerMiPosicionGPS();

            if (location != null) {
                measure = new Measure (
                        resultadoBLE.getPPB(),
                        resultadoBLE.getTemp(),
                        location.getLongitude(),
                        location.getLatitude()
                );

                Log.d(TAG, "onRecibirResultadoBLE: ----------------------------------------");
                Log.d(TAG, "onRecibirResultadoBLE:  VALOR: " + measure.getValue());
                Log.d(TAG, "onRecibirResultadoBLE:  TEMP:  " + measure.getTemperature());
                Log.d(TAG, "onRecibirResultadoBLE:  LONG:  " + measure.getLongitude());
                Log.d(TAG, "onRecibirResultadoBLE:  LAT:   " + measure.getLatitude());
                Log.d(TAG, "onRecibirResultadoBLE: ----------------------------------------");

                // Guardar resultado mediante API REST
                Log.d(TAG, "onRecibirResultadoBLE: JSON" + measure.toJSON());

                /*****************************************************************************/
                /*****************************************************************************/

                enviarMedicion(measure);
                calibrarMedicion(measure);
            }
        }
    };
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Constructor
     *
     * @param mContext link a MainActivity
     */
    /**********************************************************************************************/
    public LogicaServicio(Context mContext) {
        Log.d(TAG, "LogicaServicio: llamada al constructor");
        this.mContext = mContext;
        miReceptorBLE = new ReceptorBLE(mContext, recibirResultadoBLE);
        localizadorGPS = new LocalizadorGPS(mContext);

        /***************iniciamos la lista de medidas de la jornada****************/
        medidasDeHoy= new ArrayList<>();
    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Crea dos tareas para escanear en busca de datos del sensor por bluetooth
     * Una cada 10 intervalos
     * Otra cada intervalo solo si me he movido
     */
    /**********************************************************************************************/
    public void iniciarEscaneo() {

        // TEMPORAL: Prueba de filtrar por numSerie
        String miSensor = PreferenceManager.getDefaultSharedPreferences(mContext)
                .getString(SENSOR_SERIAL_NUMBER, "000000000000");

        filtrarSensorPorNumeroSerie(miSensor);

        Log.d(TAG, "iniciarEscaneo: método iniciado");

        // Declaro el timer
        Timer timer = new Timer();

        // Tareas que se ejecuta cada 10 intervalos
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "run: TAREA 1");
                miReceptorBLE.actualizarMediciones();
                }
        }, 0, interval * 10);

        // Tarea que se ejecuta cada intervalo solo si me he movido
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Log.d(TAG, "run: TAREA 2");
                if (meHeMovido()) {
                    miReceptorBLE.actualizarMediciones();
                }
            }
        }, interval, interval);
    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Recibe el numero de serie del sensor y lo manda al receptorBLE para crear el filtro
     *
     * @param numeroSerie String con el numero de serie del sensor
     */
    /**********************************************************************************************/
    public void filtrarSensorPorNumeroSerie(String numeroSerie) {
        Log.d(TAG, "filtrarSensorPorNumeroSerie: llamada al método");
        miReceptorBLE.setNumeroSerieSensor(numeroSerie);
    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Devuelve verdadero si me he movido 10 metros desde la última medida tomada
     *
     * @return verdadero o falso
     */
    /**********************************************************************************************/
    private Boolean meHeMovido() {

        Log.d(TAG, "meHeMovido: comprobando");
        Boolean resultado = false;
        Location actual = localizadorGPS.obtenerMiPosicionGPS();
        if (location != null) {
            // devuelve true si se cumple
            resultado = (location.distanceTo(actual) > 10);
        }
        location = actual;
        if (resultado) Log.d(TAG, "meHeMovido: SI ME HE MOVIDO");
        else Log.d(TAG, "meHeMovido: NO ME HE MOVIDO");
        return resultado;
    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Recibe una medida y la manda al servidor mediante API REST
     *
     * @param measure contiene la última medida con la posición
     */
    /**********************************************************************************************/
    public void enviarMedicion( Measure measure ) {

        PeticionarioREST elPeticionario = new PeticionarioREST();

        elPeticionario.hacerPeticionREST("POST",
                "http://vps707355.ovh.net:3000/measures",
                measure.toJSON(),
                new PeticionarioREST.Callback () {
                    @Override
                    public void respuestaRecibida( int codigo, String cuerpo ) {
                        Log.d(TAG, "Logica.enviarMedicionDeAlgo() respuestaRecibida: codigo = "
                                + codigo + " cuerpo=" + cuerpo);
                    }
                },
                "application/json; charset=UTF-8"
        );

    } // ()
    /**********************************************************************************************/

    /*******************************************************************************************************************/
/**
 * Recibe una medida y la multiplica por el factor diario de calibración.
 * Usa la medida para recalcular el factor de calibración.
 *
 * factorDeCalibracion contiene el factor diario de calibración
 * medidasDeHoy es una lista que contiene el valor de las medidas tomadas por el usuario en la jornada
 *
 *  autor: Víctor Blanco Bataller
 */
    /**********************************************************************************************/

    public void calibrarMedicion( Measure myMeasure ) {
        PeticionarioREST elPeticionario = new PeticionarioREST();

        medidasDeHoy.add(medidasContador,myMeasure.getValue());
        medidasContador++;



        elPeticionario.hacerPeticionREST("GET",
                "http://vps707355.ovh.net:3000/officialMeasures",
                null,
                new PeticionarioREST.Callback () {
                    @Override
                    public void respuestaRecibida( int codigo, String cuerpoRecibido ) {

                        try {

                            JSONArray cuerpo = new JSONArray(cuerpoRecibido);

                            double mediaOficial = 0;

                            String gasType = "";
                            switch (myMeasure.getGasType()) {
                                case "CO":
                                    gasType = "co";
                                    break;
                                case "SO2":
                                    gasType = "so2";
                                    break;
                                case "NO3":
                                    gasType = "no3";
                                    break;
                                case "O3":
                                    gasType = "o3";
                                    break;

                            }//switch

                            for (int i = 0; i < cuerpo.length(); i++) {

                                mediaOficial += Double.valueOf(cuerpo.getJSONObject(i).getString(gasType));

                            }//for()

                            mediaOficial = mediaOficial / cuerpo.length();

                            Log.d(TAG, "medida sin calibrar: " + myMeasure.getValue());

                            //si la lista de medidas mide mas de 10 calcular su media e igualar el factor a esto entre la media oficial



                            if (medidasDeHoy.size() > 10) {

                                //int[] medidasBajas= new int[Math.round(medidasDeHoy.length/10)]

                                double mediaMedidas = 0;

                                for (int i = 0; i < medidasDeHoy.size(); i++) {

                                    mediaMedidas += medidasDeHoy.get(i);

                                }//for()

                                mediaMedidas = mediaMedidas / medidasDeHoy.size();

                                factorDeCalibracion = mediaOficial / mediaMedidas;


                                measure.setValue((int) Math.round(myMeasure.getValue() * factorDeCalibracion));
                            }//if
                            //------------
                            Log.d(TAG, "Logica.calibrarMedicion() respuestaRecibida: codigo = "
                                    + codigo + " cuerpo=" + cuerpo + "factor de calibración: " + factorDeCalibracion + "+medida calibrada: " + myMeasure.getValue());
                            measure = new Measure(myMeasure.getValue(), measure.getTemperature(), measure.getLongitude(), measure.getLatitude());






                         } catch (JSONException e) {
                        //some exception handler code.
                    }
                }//respuestaRecibida()
                },
                "application/json; charset=UTF-8"
        );

    }//()
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Declaración de la interfaz para recibir los resultados del escaner bluetooth
     *
     * @param resultadoBLE Contiene el resultado recibido por el escaner BT
     */
    @Override
    public void onRecibirResultadoBLE(ResultadoBLE resultadoBLE) {
    }
    /**********************************************************************************************/

}
/**************************************************************************************************/
/**************************************************************************************************/
