package app.example.org.e5app.services;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Looper;

/**
 * Clase LocalizadorGPS para obtener las coordenadas geográficas
 */
public class LocalizadorGPS implements LocationListener {

    private static final String TAG = MeasureUpdatesService.class.getSimpleName();

    private Context mContext;
    private LocationManager miLocationManager = null;

    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;

    private Location location;

    public LocalizadorGPS(Context mContext) {
        this.mContext = mContext;
        if (miLocationManager == null) {
            miLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        }
        //LocationProvider provider = miLocationManager.getProvider(LocationManager.GPS_PROVIDER);
        //obtenerMiPosicionGPS();   // Guarda la posición al crear el objeto
    }

    public Location obtenerMiPosicionGPS() {
        try {
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            miLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            miLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);
            if (miLocationManager != null) {
                location = miLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (location != null) {
                    double latitude = location.getLatitude();
                    double longitude = location.getLongitude();
                }
            }
        }
        catch(SecurityException e) {
            e.printStackTrace();
        }
        return location;
    }

    @Override
    public void onLocationChanged(Location location) {
        //Log.e(TAG, "onLocationChanged: " + location);
        //lastLocation.set(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

}
