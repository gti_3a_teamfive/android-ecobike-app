package app.example.org.e5app.ui.Login;

//Autor: Aarón Cabrera Mir

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import app.example.org.e5app.R;


public class LoginFragment extends Fragment {

    private LoginViewModel loginViewModel;

    public boolean usuarioIncorrecto;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        loginViewModel =
                ViewModelProviders.of(this).get(LoginViewModel.class);

        View root = inflater.inflate(R.layout.fragment_login, container, false);


        //---------------------------------------
        //---------------------------------------

        //EditText para obtener datos del usuario

        final EditText usuario = (EditText) root.findViewById(R.id.editarPerfilContraseña);
        final EditText pass = (EditText) root.findViewById(R.id.confirmarPass);

        //Textos para indicar si el usuario se ha equivocado

        final TextView textoError = (TextView) root.findViewById(R.id.texto_error);
        final ImageView iconoError = (ImageView) root.findViewById(R.id.icono_error);

        //Texto para ir al signUp

        TextView registro = (TextView) root.findViewById(R.id.texto_Sign_Up);

        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.signUpFragment);
            }
        });

        //Botón inicio de sesión

        Button botonInicio = (Button) root.findViewById(R.id.botonGuardar);

        botonInicio.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                //Guardamos la información de los EditText en dos variables tipo String

                String mipass = pass.getText().toString();
                String miusuario = usuario.getText().toString();

                //Llamamos a "UsuarioIncorrecto" y le pasamos las variables guardadas

                UsuarioIncorrecto(miusuario,mipass);

                //Si "usuarioIncorrecto" es verdadero, el usuario es incorrecto, en caso contrario es correcto

                if (usuarioIncorrecto == true){

                    //Se equivoca -> texto error visible

                    textoError.setVisibility(View.VISIBLE);
                    iconoError.setVisibility(View.VISIBLE);

                } else {

                    //NO se equivoca -> texto error invisible

                    textoError.setVisibility(View.INVISIBLE);
                    iconoError.setVisibility(View.INVISIBLE);

                    //Le dejamos pasar al apartado perfil

                    Navigation.findNavController(v).navigate(R.id.action_navigation_login_to_perfilFragment);

                }

            }
        });

        //El texto de error por defecto es invisible, no se puede equivocar si aún no ha introducido nada el uasuario

        textoError.setVisibility(View.INVISIBLE);
        iconoError.setVisibility(View.INVISIBLE);

        return root;
    }


    public void UsuarioIncorrecto(String usuario, String pass){


        //Si el usuario es correcto, "usuarioIncorrecto" es false porque NO se ha equivocado, en caso contrario
        //es true porque se ha equivocado.

        if (usuario.equals("pepe") && pass.equals("1234") ){

            usuarioIncorrecto = false;

        } else {

            usuarioIncorrecto = true;

        }

    }

}