package app.example.org.e5app.ui.Mapa;

//Autor: Aarón Cabrera Mir

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import app.example.org.e5app.R;

public class MapaFragment extends Fragment {

    private MapaViewModel mapaViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mapaViewModel =
                ViewModelProviders.of(this).get(MapaViewModel.class);
        View root = inflater.inflate(R.layout.fragment_mapa, container, false);

        WebView mWebView = (WebView) root.findViewById(R.id.vistaMapa);

        FloatingActionButton fab = root.findViewById(R.id.botonInfo);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder miDialogo = new AlertDialog.Builder(getActivity());

                miDialogo.setView(getLayoutInflater().inflate(R.layout.cardview_layout_ayuda, null));
                miDialogo.setIcon(R.drawable.ic_info_green_24dp);
                miDialogo.setTitle(R.string.perfil_ayuda);
                miDialogo.setNeutralButton("OK",null);
                miDialogo.show();
            }
        });

        //

        mWebView.clearCache(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl("http://vps707355.ovh.net:3000/mapa.html");
        //mWebView.loadUrl("https://leafletjs.com/examples/mobile/example.html");

        return root;
    }

}
