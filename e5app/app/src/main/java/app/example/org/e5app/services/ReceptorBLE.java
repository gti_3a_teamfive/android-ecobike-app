/***************************************************************************************************
 *      RECEPTOR BLUETOOTH LOW ENERGY
 *
 *      Clase encargada del escaneo de beacons por bluetooth
 *      Escanea BT en busca de beacons mediante un filtro por UUID
 *      Manda los resultados coincidentes al interfaz de la logica
 *
 *      autor: Joan Lluís Fuster
 **************************************************************************************************/

package app.example.org.e5app.services;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**************************************************************************************************/

/**************************************************************************************************/
/**
 * Clase ReceptorBLE: escanea dispositivos BT, filtra por UUID y extrae las medidas
 */
/**************************************************************************************************/
public class ReceptorBLE {

    private static final String TAG = MeasureUpdatesService.class.getSimpleName();

    private Context mContext;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothLeScanner bluetoothLeScanner;
    private ScanCallback mScanCallback;
    private Handler mHandler;

    private RecibirResultadoBLE recibirResultadoBLE;

    private static final long SCAN_PERIOD = 5000;      // 5 segundos de escaneo

    private byte[] numeroSerieSensor = {0, 0, 0, 0, 0, 0};    // para filtrar las medidas por sensor

    /**********************************************************************************************/
    /**
     * Constructor
     *
     * @param mContext Recibe el context de MainActivity
     * @param recibirResultadoBLE Es el interfaz para mandar el resultado
     */
    /**********************************************************************************************/
    public ReceptorBLE(Context mContext, RecibirResultadoBLE recibirResultadoBLE) {
        Log.d(TAG,"Iniciado constructor");

        this.mContext = mContext;
        // This is the interface for the callback
        this.recibirResultadoBLE = recibirResultadoBLE;
        // handler for scanning event
        mHandler = new Handler();

        // Setting up bluetooth
        final BluetoothManager bluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();

        if (bluetoothAdapter == null) {
            // Device does not support Bluetooth
            // Toast.makeText(mContext,"Bluetooth Not Supported",Toast.LENGTH_SHORT).show();
        }

        bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();

        //actualizarMediciones();   // para tener una medida guardada

        if ( !bluetoothAdapter.isEnabled()){
            Log.d(TAG, "el bluetooth no esta habilitado");
        } else {
            Log.d(TAG, "el bluetooth SI esta habilita ");
        }

    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Método público para iniciar el servicio
     */
    /**********************************************************************************************/
    public void actualizarMediciones() {
        Log.d(TAG, "actualizarMediciones: método iniciado");
        startScanning();
    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Start scanning for BLE Advertisements (& set it up to stop after a set period of time).
     */
    /**********************************************************************************************/
    private void startScanning() {
        Log.d(TAG, "llamado método para escanear bluetooth");
        if (mScanCallback == null) {
            // Will stop the scanning after a set time.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    stopScanning();
                    Log.d(TAG, "detenido el escaneo BT por TIME OUT");
                }}, SCAN_PERIOD);
            // Kick off a new scan. Return one result through mScanCallback()
            mScanCallback = new miScanCallback(mContext);
            bluetoothLeScanner.startScan(buildScanFilters(), buildScanSettings(), mScanCallback);
            Log.d(TAG, "BT escaneando dispositivos");
        }
    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Stop scanning for BLE Advertisements.
     */
    /**********************************************************************************************/
    private void stopScanning() {
        // Stop the scan, wipe the callback.
        Log.d(TAG, "Deteniendo escaneo BT");
        bluetoothLeScanner.stopScan(mScanCallback);
        mScanCallback = null;

        // Even if no new results, update 'last seen' times.
        //mAdapter.notifyDataSetChanged();
    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Recibe el numero de serie del sensor y lo almacena para después crear el filtro BT
     *
     * @param numeroSerieSensor mandado por la lógica del servicio
     */
    /**********************************************************************************************/
    public void setNumeroSerieSensor(String numeroSerieSensor) {
        Log.d(TAG, "setNumeroSerieSensor: método iniciado");
        this.numeroSerieSensor = numeroSerieSensor.getBytes();
        if (numeroSerieSensor.length() != 12) {
            throw new IllegalArgumentException(
                    "Invalid hexadecimal String supplied.");
        }

        byte[] bytes = new byte[numeroSerieSensor.length() / 2];
        for (int i = 0; i < numeroSerieSensor.length(); i += 2) {
            this.numeroSerieSensor[i / 2] = hexToByte(numeroSerieSensor.substring(i, i + 2));
        }
        Log.d(TAG, "setNumeroSerieSensor: " + Arrays.toString(this.numeroSerieSensor));
    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Crea el filtro para el escaneo BT con el Uuid del sensor
     *
     * Return a List of {@link ScanFilter} objects to filter by Service UUID.
     */
    /**********************************************************************************************/
    private List<ScanFilter> buildScanFilters() {
        List<ScanFilter> scanFilters = new ArrayList<>();

        ScanFilter.Builder builder = new ScanFilter.Builder();
        // Comment out the below line to see all BLE devices around you
        // builder.setServiceUuid(new ParcelUuid(UUID.fromString(SERVICE_UUID)));

        // the manufacturer data byte is the filter!
        // Aquí creo un filtro para el escaneo bluetooth mediante el UUID, la trama del beacon
        // y una máscara que indica los bytes que deben coincidir con el dispositivo encontrado
        final byte[] manufacturerData = new byte[]
                {
                        0, 0,
                        // uuid E2C56DB5-DFFB-48D2-B060-XXXXXXXXXXXX
                        -30, -59, 109, -75,
                        -33, -5,
                        72, -46,
                        -80, 96,
                        // sensor serial number
                        // 18, 33, 24, 1, 9, 2,
                        numeroSerieSensor[0], numeroSerieSensor[1], numeroSerieSensor[2],
                        numeroSerieSensor[3], numeroSerieSensor[4], numeroSerieSensor[5],
                        // major
                        0, 0,
                        // minor
                        0, 0,
                        0
                };

        // the mask tells what bytes in the filter need to match, 1 if it has to match, 0 if not
        final byte[] manufacturerDataMask = new byte[]
                {
                        0, 0,
                        // uuid
                        1, 1, 1, 1,
                        1, 1,
                        1, 1,
                        1, 1,
                        // serial number
                        1, 1, 1, 1, 1, 1,
                        // major
                        0,0,
                        // minor
                        0,0,
                        0
                };

        // crea el filtro. El 76 corresponde al código de fabricante de apple (ibeacon)
        // 65535 corresponde a 0xFFFF id reservado para pruebas
        builder.setManufacturerData(76, manufacturerData, manufacturerDataMask);
        scanFilters.add(builder.build());
        return scanFilters;
    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Return a {@link ScanSettings} object set to use low power (to preserve battery life).
     */
    /**********************************************************************************************/
    private ScanSettings buildScanSettings() {
        ScanSettings.Builder builder = new ScanSettings.Builder();
        builder.setScanMode(ScanSettings.SCAN_MODE_LOW_POWER);
        return builder.build();
    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Custom ScanCallback object - send to interface on success, displays error on failure.
     */
    /**********************************************************************************************/
    private class miScanCallback extends ScanCallback {

        private Context mContext;

        /******************************************************************************************/
        /**
         * Constructor del callback
         *
         * @param context MainActivity context
         */
        /******************************************************************************************/
        public miScanCallback(Context context) {
            this.mContext = context;
        }
        /******************************************************************************************/

        /******************************************************************************************/
        /**
         * Callback ejecutado por el escaner BT al encontrar un resultado positivo
         *
         * @param callbackType
         * @param result
         */
        /******************************************************************************************/
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            Log.d(TAG,"recibido resultado del escaneo BT");
            super.onScanResult(callbackType, result);
            stopScanning();   // dejo de escanear por que he obtenido un resultado coincidente
            Log.d(TAG, "onScanResult: Dejo de escanear. Tengo un resultado");
            Log.d(TAG, "onScanResult: Recibido: " + result.toString());
            Log.d(TAG, "onScanResult: ScanRecord: " + result.getScanRecord().toString());

            // Get the Device of the result
            final BluetoothDevice address = result.getDevice();
            // Try to get the name of the device...
            String name = "";
            try {
                name = result.getScanRecord().getDeviceName();
            } catch (NullPointerException e) {
                Log.e(TAG, "Device-Name could not be read!");
            }
            // Get the RSSI of the device
            int rssi = result.getRssi();
            // Recupero major y minor
            byte[] miTramaEnBytes = result.getScanRecord().getManufacturerSpecificData(76);
            int major  = ((miTramaEnBytes[18] & 0xff) << 8) | (miTramaEnBytes[19] & 0xff);
            int minor  = ((miTramaEnBytes[20] & 0xff) << 8) | (miTramaEnBytes[21] & 0xff);

            ResultadoBLE resultadoBLE = new ResultadoBLE(address, name, rssi, major, minor);
            Log.d(TAG, "onScanResult: Creado resultadoBLE: " + resultadoBLE.toString());
            Log.d(TAG, "onScanResult: address: " + resultadoBLE.getAddress());
            Log.d(TAG, "onScanResult: name: " + resultadoBLE.getName());
            Log.d(TAG, "onScanResult: rssi: " + resultadoBLE.getRssi());
            Log.d(TAG, "onScanResult: major: " + resultadoBLE.getMajor());
            Log.d(TAG, "onScanResult: minor: " + resultadoBLE.getMinor());
            recibirResultadoBLE.onRecibirResultadoBLE(resultadoBLE);
        }
        /******************************************************************************************/

        /******************************************************************************************/
        /**
         * Llamado si hay un error en el escaneo
         *
         * @param errorCode
         */
        /******************************************************************************************/
        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.e(TAG, "ERROR: BT Scan Failed!!! Error Code: " + errorCode);
        }
        /******************************************************************************************/
    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Interfaz
     */
    /**********************************************************************************************/
    public interface RecibirResultadoBLE {
        void onRecibirResultadoBLE(ResultadoBLE resultadoBLE);
    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Función auxiliar para convertir en bytes el string del numero de serie recibido de dos en dos
     *
     * @param hexString recibe un String de dos caracteres hexadecimales
     * @return los convierte a un byte
     */
    /**********************************************************************************************/
    public byte hexToByte(String hexString) {
        int firstDigit = toDigit(hexString.charAt(0));
        int secondDigit = toDigit(hexString.charAt(1));
        return (byte) ((firstDigit << 4) + secondDigit);
    }
    /**********************************************************************************************/

    /**********************************************************************************************/
    /**
     * Función auxiliar para convertir caracteres hexadecimales a enteros
     *
     * @param hexChar
     * @return
     */
    /**********************************************************************************************/
    private int toDigit(char hexChar) {
        int digit = Character.digit(hexChar, 16);
        if(digit == -1) {
            throw new IllegalArgumentException(
                    "Invalid Hexadecimal Character: "+ hexChar);
        }
        return digit;
    }
    /**********************************************************************************************/

}
/**************************************************************************************************/
/**************************************************************************************************/
