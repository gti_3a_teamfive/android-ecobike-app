package app.example.org.e5app.ui.Mapa;

//Autor: Aarón Cabrera Mir

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MapaViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public MapaViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Mapa fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}