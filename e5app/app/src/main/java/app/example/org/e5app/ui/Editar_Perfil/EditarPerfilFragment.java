package app.example.org.e5app.ui.Editar_Perfil;

// ----------------------------------------------------------------------------------
// Autor: Manuel Mouzo Marsella,
// Equipo: 5
// Descripcion: codigo en el que se llaman a funciones para cambiar datos del usuario.
// Fecha:
// ----------------------------------------------------------------------------------

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.IOException;
import java.util.List;

import androidx.navigation.Navigation;
import app.example.org.e5app.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditarPerfilFragment extends Fragment {

    public static final int REQUEST_IMAGE = 100;

    private EditarPerfilViewModel EditarPerfilView;

    @BindView(R.id.editar_perfil_imagen_perfil) ImageView imgProfile;

    public static EditarPerfilFragment newInstance() {
        return new EditarPerfilFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        EditarPerfilView =
                ViewModelProviders.of(this).get(EditarPerfilViewModel.class);
        View root = inflater.inflate(R.layout.fragment_editar_perfil, container, false);

        ButterKnife.bind(this, root);

        loadProfileDefault();

        // Clearing older images from cache directory
        // don't call this line if you want to choose multiple images in the same activity
        // call this once the bitmap(s) usage is over

        ImagePickerActivity.clearCache(getContext());

        Button botonGuardar = (Button) root.findViewById(R.id.botonGuardar);

        botonGuardar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_editarPerfilFragment3_to_perfilFragment);
            }
        });
        return root;
    }

    //----------------------------------------------------------------------------------------------
    //	String ->
    //		loadProfile()
    //
    //----------------------------------------------------------------------------------------------
    private void loadProfile(String url) {

        Log.d("Test", "Iniciado metodo loadProfile");

        Log.d("LOG", "Image cache path: " + url);
        GlideApp.with(this).load(url)
                .into(imgProfile);

        imgProfile.setColorFilter(ContextCompat.getColor(getContext(), android.R.color.transparent));

        Log.d("Test", "Finalizado metodo loadProfile");
    }


    private void loadProfileDefault() {
        Log.d("Test", "Iniciado metodo loadProfileDefault");

        GlideApp.with(this)
                .load(R.drawable.ic_user)
                .into(imgProfile);

        Log.d("Test", "Finalizado metodo loadProfileDefault");
    }


    //-----------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------
    @OnClick({R.id.editar_perfil_imagen_perfil, R.id.editar_perfil_add_image})
    void onProfileImageClick() {
        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }


    private void showImagePickerOptions() {
        Log.d("Test", "Iniciado metodo showImagePickerOptions");
        int identificador = 1;
        ImagePickerActivity.showImagePickerOptionsBuilder(getContext(), identificador, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });

        Log.d("Test", "Finalizado metodo showImagePickerOptions");
    }

    //-----------------------------------------------
    //		launchCameraIntent();
    //			-> Intent, Int
    //-----------------------------------------------
    private void launchCameraIntent() {
        Log.d("Test", "Iniciado metodo launchCameraIntent");

        Intent intent = new Intent(getContext(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
        Log.d("Test", "Finalizado metodo launchCameraIntent");
    }

    //-----------------------------------------------
    //		launchGalleryIntent();
    //			-> Intent, Int
    //-----------------------------------------------
    private void launchGalleryIntent() {
        Log.d("Test", "Iniciado metodo launchGalleryIntent");

        Intent intent = new Intent(getContext(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);

        Log.d("Test", "Finalizado metodo launchGalleryIntent");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                try {
                    // Este es el bitmap que se podria guardar en el servidor
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);

                    // Se carga la imagen guardada en la cache
                    loadProfile(uri.toString());

                    Log.d("DEBUG", uri.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showSettingsDialog() {
        Log.d("Test", "Iniciado metodo showSettingsDialog");

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.editar_perfil_dialog_permission_title));
        builder.setMessage(getString(R.string.editar_perfil_dialog_permission_message));
        builder.setPositiveButton(getString(R.string.editar_perfil_go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

        Log.d("Test", "Finalizado metodo showSettingsDialog");
    }

    private void openSettings() {
        Log.d("Test", "Iniciado metodo openSettings");

        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);

        Log.d("Test", "Finalizado metodo openSettings");
    }
}
