package app.example.org.e5app.models;

import com.google.gson.Gson;

/**
 * Class Measure: clase POJO que representa las medidas geolocalizadas
 */
public class Measure {
    private String gasType;
    private int value;
    private int temperature;
    private int humidity;
    private double longitude;
    private double latitude;
    //private Long hora;       // de momento la creo al mandar a la base de datos
    private String userId;

    public Measure() {
    }

    public Measure(int value, int temperature, double longitude, double latitude) {
        this.gasType = "CO";    // de momento lo pongo así
        this.value = value;
        this.temperature = temperature;
        this.humidity = 45;
        this.longitude = longitude;
        this.latitude = latitude;
        //this.hora = System.currentTimeMillis();
        this.userId = "5de7b92bd393950012b14ee5";    // ToDo: coger userId del login
    }

    public String getGasType() {
        return gasType;
    }

    public int getValue() {
        return value;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    /*
    public Long getHora() {
        return (TimeUnit.MILLISECONDS.toSeconds(hora));
    }
    */

    public String getUserId() {
        return userId;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String toJSON() {
        Gson gson = new Gson();
        String json = gson.toJson(this);
        return json;
    }

}
