package app.example.org.e5app.services;

import android.bluetooth.BluetoothDevice;

public class ResultadoBLE {

    private BluetoothDevice address;
    private String name;
    private int rssi;
    private int major;
    private int minor;

    public ResultadoBLE() {
    }

    public ResultadoBLE(BluetoothDevice address, String name, int rssi, int major, int minor) {
        this.address = address;
        this.name = name;
        this.rssi = rssi;
        this.major = major;
        this.minor = minor;
    }

    public int getTemp() {
        return (major >> 8) & 0xff;
    }

    public int getPPB() {
        int ppb = (major & 0xff);
        ppb = (ppb << 16) | (minor & 0xffff);
        return ppb;
    }

    public BluetoothDevice getAddress() {
        return address;
    }

    public void setAddress(BluetoothDevice address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }
}
