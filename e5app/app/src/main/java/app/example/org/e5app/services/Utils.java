package app.example.org.e5app.services;

import android.content.Context;
import android.preference.PreferenceManager;

import java.text.DateFormat;
import java.util.Date;

import app.example.org.e5app.R;
import app.example.org.e5app.models.Measure;

public class Utils {

    static final String KEY_REQUESTING_MEASURE_UPDATES = "requesting_measure_updates";

    /**
     * Returns true if requesting measure updates, otherwise returns false.
     *
     * @param context The {@link Context}.
     */
    public static boolean requestingMeasureUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_REQUESTING_MEASURE_UPDATES, false);
    }

    /**
     * Stores the measure updates state in SharedPreferences.
     * @param requestingMeasureUpdates The measure updates state.
     */
    static void setRequestingMeasureUpdates(Context context, boolean requestingMeasureUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_REQUESTING_MEASURE_UPDATES, requestingMeasureUpdates)
                .apply();
    }

    /**
     * Returns the {@code measure} object as a human readable string.
     * @param measure  The {@link Measure}.
     */
    static String getMeasureText(Measure measure) {
        return measure == null ? "Unknown measure" :
                "(" + measure.getLatitude() + ", " + measure.getLongitude() + ")";
    }

    static String getMeasureTitle(Context context) {
        return context.getString(R.string.measure_updated,
                DateFormat.getDateTimeInstance().format(new Date()));
    }
}
