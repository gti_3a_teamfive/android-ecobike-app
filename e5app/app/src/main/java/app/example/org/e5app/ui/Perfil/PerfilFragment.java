package app.example.org.e5app.ui.Perfil;

//Autor: Manuel Mouzo Marsella, Joan Lluís Fuster, Aarón Cabrera

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import app.example.org.e5app.MainActivity;
import app.example.org.e5app.R;
import app.example.org.e5app.ui.Editar_Perfil.GlideApp;
import app.example.org.e5app.ui.Editar_Perfil.ImagePickerActivity;
import butterknife.BindView;
import butterknife.ButterKnife;


import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import app.example.org.e5app.services.PeticionarioREST;
import app.example.org.e5app.models.Measure;

import static com.yalantis.ucrop.UCropFragment.TAG;

public class PerfilFragment extends Fragment{

    private PerfilViewModel perfilView;

    private BluetoothAdapter bluetoothAdapter;

    private Measure measure;
    private Measure measures[];

    static final String SENSOR_SERIAL_NUMBER = "sensor_serial_number";

    // UI elements.
    private Button mRequestMeasureUpdatesButton;
    private Button mRemoveMeasureUpdatesButton;
    private Button botonEscanear;
    private Button botonActualizarHistorial;
    private TextView textoCalidad;
    private TextView textoAqi;
    private Button botonContaminacion;
    private TextView textoQr;
    private String qrId;
    private TextView editTextQr;
    private boolean buttonCheck = true;
    private String textoParaGuardar;
    public static final int REQUEST_IMAGE = 100;


    private void arrancarBluetooth() {

    }

    @BindView(R.id.imagenContaminacion) ImageView imgContaminacion;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        perfilView =
                ViewModelProviders.of(this).get(PerfilViewModel.class);
        View root = inflater.inflate(R.layout.fragment_perfil, container, false);

        ButterKnife.bind(this, root);

        TextView editarPerfil = (TextView) root.findViewById(R.id.editarPerfil);

        editarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_perfilFragment_to_editarPerfilFragment3);
            }
        });

        // Initializes Bluetooth adapter.
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        mRequestMeasureUpdatesButton = (Button) root.findViewById(R.id.botonEmpezarMedir);
        mRemoveMeasureUpdatesButton = (Button) root.findViewById(R.id.botonPararMedir);

        mRequestMeasureUpdatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((MainActivity)getActivity()).checkPermissions()) {
                    // Ensures Bluetooth is available on the device and it is enabled.
                    // If not, displays a dialog requesting user permission to enable Bluetooth.
                    if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, 1);
                    }
                    ((MainActivity)getActivity()).mService.requestMeasureUpdates();
                } else {
                    ((MainActivity)getActivity()).requestPermissions();
                }
            }
        });

        botonEscanear = (Button) root.findViewById(R.id.botonEscanearQr);
        botonContaminacion = (Button) root.findViewById(R.id.botonEscanearContaminacion);
        textoAqi =( TextView) root.findViewById(R.id.textoAqiContaminacion);
        textoCalidad =( TextView) root.findViewById(R.id.textoCalidadContaminacion);
        textoQr = (TextView) root.findViewById(R.id.textoId);

        botonEscanear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                escanear();
            }
        });
        botonContaminacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contaminacionOnClick();
            }
        });

        mRemoveMeasureUpdatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).mService.removeMeasureUpdates();
            }
        });

        // Actualización apartado historial

        botonActualizarHistorial = (Button) root.findViewById(R.id.botonActualizarHist);

        botonActualizarHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                actualizarHistorial();
            }
        });


        return root;
    }

    //-----------------------------------------------
    //		escanear();
    //-----------------------------------------------

    public void escanear() {
        Log.d("Test", "Iniciado metodo escanear");
        IntentIntegrator intent = IntentIntegrator.forSupportFragment(PerfilFragment.this);
        //IntentIntegrator intent = new IntentIntegrator(getActivity());
        intent.setCaptureActivity(ZxingCameraOrientation.class);
        intent.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        intent.setPrompt(getString(R.string.qr_textoHint));
        intent.setCameraId(0);
        intent.setOrientationLocked(false);
        intent.setBeepEnabled(false);
        intent.setBarcodeImageEnabled(false);
        intent.initiateScan();
        Log.d("Test", "Terminado metodo escanear");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d("PRUEBA-----------PRUEBA", Integer.toString(requestCode));
        if (requestCode == 49374){
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        Log.d("Test", "Iniciado requestCode == 49374");

            if(result != null) {
                if(result.getContents() == null) {
                    Toast.makeText(getContext(),getString(R.string.qr_textoCancelar) , Toast.LENGTH_SHORT).show();
                } else {
                    qrId = result.getContents().toString();
                    textoQr.setText(qrId);
                    //((MainActivity)getActivity()).mService.logica.filtrarSensorPorNumeroSerie(qrId);
                    PreferenceManager.getDefaultSharedPreferences(((MainActivity)getActivity()))
                            .edit()
                            .putString(SENSOR_SERIAL_NUMBER, qrId)
                            .apply();
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
            Log.d("Test", "Finalizado requestCode == 49374");
        }

        if (requestCode == REQUEST_IMAGE) {
            Log.d("Test", "Iniciado requestCode == REQUEST_IMAGE");

            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                try {
                    // Este es el bitmap que se podria guardar en el servidor
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), uri);

                    // Se carga la imagen guardada en la cache
                    loadPicture(uri.toString());

                    Log.d("DEBUG", uri.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Log.d("Test", "Finalizado requestCode == REQUEST_IMAGE");
        }

    }
    //-----------------------------------------------
    //      contaminacionOnClick()
    //-----------------------------------------------

    void contaminacionOnClick() {
        Log.d("Test", "Iniciado metodo contaminacionOnClick");

        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
        Log.d("Test", "Iniciado metodo contaminacionOnClick");
    }

    private void loadPicture(String url) {
        getRndMeassure();

        GlideApp.with(this).load(url)
                .into(imgContaminacion);
        imgContaminacion.setColorFilter(ContextCompat.getColor(getContext(), android.R.color.transparent));
    }


    private void showImagePickerOptions() {
        Log.d("Test", "Iniciado metodo showImagePickerOptions");
        int identificador = 2;
        ImagePickerActivity.showImagePickerOptionsBuilder(getContext(), identificador, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });

        Log.d("Test", "Finalizado metodo showImagePickerOptions");
    }

    //-----------------------------------------------
    //		launchCameraIntent();
    //			-> Intent, Int
    //-----------------------------------------------

    private void launchCameraIntent() {
        Log.d("Test", "Iniciado metodo launchCameraIntent");

        Intent intent = new Intent(getContext(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);

        Log.d("Test", "Finalizado metodo launchCameraIntent");
    }

    //-----------------------------------------------
    //		launchGalleryIntent();
    //			-> Intent, Int
    //-----------------------------------------------

    private void launchGalleryIntent() {
        Log.d("Test", "Iniciado metodo launchGalleryIntent");

        Intent intent = new Intent(getContext(), ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        startActivityForResult(intent, REQUEST_IMAGE);

        Log.d("Test", "Finalizado metodo launchGalleryIntent");
    }

    private void showSettingsDialog() {
        Log.d("Test", "Iniciado metodo showSettingsDialog");
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.editar_perfil_dialog_permission_title));
        builder.setMessage(getString(R.string.editar_perfil_dialog_permission_message));
        builder.setPositiveButton(getString(R.string.editar_perfil_go_to_settings), (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

        Log.d("Test", "Finalizado metodo showSettingsDialog");
    }

    private void openSettings() {
        Log.d("Test", "Iniciado metodo openSettings");

        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);

        Log.d("Test", "Finalizado metodo openSettings");
    }

    private void getRndMeassure(){
        Log.d("Test", "Iniciado metodo getRndMeassure");
        int[] numberArray = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,3,3,3,3,3,4,4,4,5,5,6};
        int rnd = new Random().nextInt(numberArray.length);
        double Aqi = 0;
        if(numberArray[rnd]==1){
            Aqi = rndBetweenValues(1,50);
            textoCalidad.setText(getResources().getString(R.string.contaminacion_texto_Calidad_1));
            textoCalidad.setTextColor(getResources().getColor(R.color.colorContaminacionBaja));
        };
        if(numberArray[rnd]==2){
            Aqi = rndBetweenValues(51,100);
            textoCalidad.setText(getResources().getString(R.string.contaminacion_texto_Calidad_1));
            textoCalidad.setTextColor(getResources().getColor(R.color.colorContaminacionBaja));
        };
        if(numberArray[rnd]==3){
            Aqi = rndBetweenValues(101,150);
            textoCalidad.setText(getResources().getString(R.string.contaminacion_texto_Calidad_2));
            textoCalidad.setTextColor(getResources().getColor(R.color.colorContaminacionMedia));
        };
        if(numberArray[rnd]==4){
            Aqi = rndBetweenValues(201,300);
            textoCalidad.setText(getResources().getString(R.string.contaminacion_texto_Calidad_2));
            textoCalidad.setTextColor(getResources().getColor(R.color.colorContaminacionMedia));
        }
        if(numberArray[rnd]==5){
            Aqi = rndBetweenValues(151,200);
            textoCalidad.setText(getResources().getString(R.string.contaminacion_texto_Calidad_3));
            textoCalidad.setTextColor(getResources().getColor(R.color.colorContaminacioAlta));
        };
        if(numberArray[rnd]==6){
            Aqi = rndBetweenValues(301,500);
            textoCalidad.setText(getResources().getString(R.string.contaminacion_texto_Calidad_3));
            textoCalidad.setTextColor(getResources().getColor(R.color.colorContaminacioAlta));
        };
        textoAqi.setText(Double.toString(Aqi));
        Log.d("Test", "Finalizado metodo getRndMeassure");

    }

    private double rndBetweenValues(int max, int min){
        Log.d("Test", "Iniciado metodo rndBetweenValues");
        return Math.floor(Math.random()*(max-min+1)+min);
    }

    public void actualizarHistorial(){

        Log.d("HISTORIAL_BOTON","Pasa por la funcion");

        //Inicializamos variable measure.

        measure = new Measure();

        PeticionarioREST elPeticionario = new PeticionarioREST();

        elPeticionario.hacerPeticionREST("GET",
                "http://vps707355.ovh.net:3000/measures",null,
                new PeticionarioREST.Callback () {
                    @Override
                    public void respuestaRecibida( int codigo, String cuerpo ) {
                        Log.d("HISTORIAL_RESP_RECIBIDA", "actualizarHistorial() respuestaRecibida: codigo = "
                                + codigo + " cuerpo=" + cuerpo);

                        Toast.makeText(getActivity(), "Datos actualizados",
                                Toast.LENGTH_LONG).show();

                    }
                },
                "application/json; charset=UTF-8"
        );

    }
}
