package app.example.org.e5app.ui.Sign_Up;

//Autor: Manuel Mouzo Marsella

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SignUpViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public SignUpViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is SignUp fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
